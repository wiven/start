var gulp            = require('gulp');
var sass            = require('gulp-sass');
var notify          = require('gulp-notify');
var connect         = require('gulp-connect-php');
var cssmin          = require('gulp-cssmin');
var rename          = require('gulp-rename');
var minify          = require('gulp-minify');
var eslint          = require('gulp-eslint');
var gutil           = require('gulp-util');
var ftp             = require('vinyl-ftp');
var browserSync     = require('browser-sync').create();
var serve           = require('gulp-serve');
var runSequence     = require('run-sequence');
var babel           = require("gulp-babel");
var concat          = require('gulp-concat');
var uglify          = require('gulp-uglify');

var config = {
    jsPath: './dev/js/modules',
    sassPath: './dev/scss'
};

gulp.task('sass', function(){
    return gulp.src(config.sassPath + '/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('app/css'))
        .pipe(notify({ message: 'CSS ready!' }))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: 'app'
        },
        port: 9160
    });
    browserSync.notify('Browser bijgewerkt!');
});


gulp.task('watch', ['browserSync', 'sass', 'scripts', 'lint'], function(){
    gulp.watch(config.sassPath + '/**/*.scss', ['sass']);
    gulp.watch('app/**/*.html', browserSync.reload);
    gulp.watch(config.jsPath + '/*.js', ['scripts', browserSync.reload]);
});

gulp.task('php', function() {
    connect.server({ base: 'app', port: 8010, keepalive: true});
});

gulp.task('css-minify', function () {
    gulp.src(['app/css/*.css', '!app/css/*.min.css'])
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('app/css/min'))
        .pipe(notify({ message: 'CSS minified!' }));
});

gulp.task('js-minify', function() {
    gulp.src(['app/js/*.js', '!app/js/*.min.js'])
        .pipe(minify({
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '.min.js']
        }))
        .pipe(gulp.dest('app/js/min'))
        .pipe(notify({ message: 'JS minified!' }));
});

gulp.task('lint', function() {
    return gulp.src('app/js/**').pipe(eslint({
        'rules':{
            'quotes': [1, 'single'],
            'semi': [1, 'always']
        }
    }))
        .pipe(eslint.format())
        .pipe(eslint.failOnError());
});

gulp.task('lint-css', function lintCssTask() {
    const gulpStylelint = require('gulp-stylelint');

    return gulp
        .src([config.sassPath + '/*.scss', '!' + config.sassPath + '/_reset.scss'])
        .pipe(gulpStylelint({
            reporters: [
                {
                    formatter: 'string',
                    console: true
                }
            ]
        }));
});

gulp.task('build-production', ['lint', 'js-minify', 'css-minify'], function() {
    console.log('Building for production');
});

// Concatenate JS task
gulp.task('scripts', function() {
    return gulp.src(config.jsPath + '/*.js')
        .pipe(babel())
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('./app/js/'))
        .pipe(notify({ message: 'Scripts task complete' }));
});

gulp.task('build', ['lint', 'sass'], function () {
    console.log('Building for deployment');
});

gulp.task('test', ['lint', 'lint-css'], function () {
    console.log('Linting completed!');
});

gulp.task('serve', serve('app'));