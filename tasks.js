var FtpDeploy = require('ftp-deploy');
var ftpDeploy = new FtpDeploy();

var config = {
    username: process.argv[2],
    password: process.argv[3],
    host: process.argv[4],
    port: 21,
    localRoot: __dirname + "/app",
    remoteRoot: process.argv[5],
    exclude: ['css/bootstrap.css', 'css/font-awesome.css', 'css/reset.css', 'css/swipebox.css', 'css/min/*', 'scss/*', 'js/min/*']
}

ftpDeploy.deploy(config, function(err) {
    if (err) console.log(err)
    else console.log('finished');
});