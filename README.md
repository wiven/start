# Start

Boilerplate for websites created by [WiVen Web Solutions](https://wiven.be)

## Local installation

* First get all the code with  `git clone https://wiven@bitbucket.org/wiven/start.git`
* Then go into the directory `cd start`
* After that install the dependencies with  `npm i`
* Finally start the watch with `npm run wiven-watch`
* Everything will go automatically, even a new browser window will open

## Features

* ES6
* Gulp
* SASS
* CI with linting
    * JavaScript
    * SASS
* CD with upload to server
* BrowserSync
* Placeholder CSS Media Queries.
* Useful CSS helper classes.
* Default print styles, performance optimized.

## URL's

* Live environment: [https://wiven.be](#!)
* Staging environment: [https://wiven.be/test](#!)


## Browser support

* Chrome *(latest 2)*
* Edge *(latest 2)*
* Firefox *(latest 2)*
* Internet Explorer 11+
* Opera *(latest 2)*
* Safari *(latest 2)*